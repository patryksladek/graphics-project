var renderer, scene, camera, pointLight,spotLight

var ball,paddle1,paddle2;
var paddleWidth = 10,
paddleHeight = 30,
paddleDepth = 10,
paddleQuality = 1;

var fieldWidth = 400, fieldHeight = 200;

var cpuScore=0,playerScore=0;

function setup()
{
  setupGameScene();
  draw();
}

function setupGameScene(){
  // set the scene size
  	var WIDTH = 640,
  	  HEIGHT = 360;

  	// set some camera attributes
  	var VIEW_ANGLE = 50,
  	  ASPECT = WIDTH / HEIGHT,
  	  NEAR = 0.1,
  	  FAR = 10000;

      var tableWidth =fieldWidth,
      tableHeight=fieldHeight,
      tableQuality=10;



  	var c = document.getElementById("gameCanvas");

  	renderer = new THREE.WebGLRenderer();
  	camera =
  	  new THREE.PerspectiveCamera(
  		VIEW_ANGLE,
  		ASPECT,
  		NEAR,
  		FAR);

  	scene = new THREE.Scene();

  	// add the camera to the scene
  	scene.add(camera);

  	camera.position.z = 320;

  	// start the renderer
  	renderer.setSize(WIDTH, HEIGHT);

  	// attach the render-supplied DOM element
  	c.appendChild(renderer.domElement);

    // set up the sphere vars
    var radius = 5,
    segments = 6,
    rings = 6;

    // create the sphere's material
    var sphereMaterial =
    new THREE.MeshLambertMaterial(
    {
    color: 0xD43001
    });

    // Create a ball with sphere geometry
        ball = new THREE.Mesh(
        new THREE.SphereGeometry(radius,
        segments,
        rings),
        sphereMaterial);

    // add the sphere to the scene
    scene.add(ball);
    ball.position.x = 0;
    ball.position.y = 0;
    ball.position.z = radius;
    ball.receiveShadow = true;

    // // create a point light
  	pointLight =
  	  new THREE.PointLight(0xF8D898);

  	// set its position
  	pointLight.position.x = -1000;
  	pointLight.position.y = 0;
  	pointLight.position.z = 1000;
  	pointLight.intensity = 2.9;
  	pointLight.distance = 10000;
  	// add to the scene
  	scene.add(pointLight);

  	// add a spot light
  	// this is important for casting shadows
      spotLight = new THREE.SpotLight(0xF8D898);
      spotLight.position.set(0, 0, 460);
      spotLight.intensity = 1.5;
      spotLight.castShadow = true;
      scene.add(spotLight);

  	// MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
  	renderer.shadowMap.enabled = true;

    //TABLE
    var tableMat = new THREE.MeshLambertMaterial({
      color:0x4BD121
    });

    var table = new THREE.Mesh(
      new THREE.PlaneGeometry(
        tableWidth*0.95,
        tableHeight,
        tableQuality,
        tableQuality),
        tableMat
      );

      scene.add(table);
      table.receiveShadow = true;
      //PADDLES

    	var paddle1Mat =
    	  new THREE.MeshLambertMaterial(
    		{
    		  color: 0x1B32C0
    		});

    	var paddle2Mat =
    	  new THREE.MeshLambertMaterial(
    		{
    		  color: 0xFF4045
    		});
        paddle1 = new THREE.Mesh(
        new THREE.CubeGeometry(
          paddleWidth,
          paddleHeight,
          paddleDepth,
          paddleQuality,
          paddleQuality,
          paddleQuality
        ),
      paddle1Mat);
      scene.add(paddle1);
      paddle1.receiveShadow = true;
      paddle1.castShadow = true;
        paddle2 = new THREE.Mesh(
        new THREE.CubeGeometry(
          paddleWidth,
          paddleHeight,
          paddleDepth,
          paddleQuality,
          paddleQuality,
          paddleQuality
        ),
      paddle2Mat);
      scene.add(paddle2);
      paddle2.receiveShadow = true;
      paddle2.castShadow = true;

      //PADDLE POSITIONS
      paddle1.position.x = -fieldWidth/2 + paddleWidth;
      paddle2.position.x = fieldWidth/2 - paddleWidth;

      paddle1.position.z = paddleDepth;
      paddle2.position.z = paddleDepth;



}


function draw()
{
  // draw THREE.JS scene
   renderer.render(scene, camera);

   //loop draw
  requestAnimationFrame(draw);

  //game logic
  ballUpdate();
  userInput();
  enemyAi();
  paddlePhysics();
  cameraPhysics();
}

// Handles camera and lighting logic
function cameraPhysics()
{
	// we can easily notice shadows if we dynamically move lights during the game
	spotLight.position.x = ball.position.x * 2;
	spotLight.position.y = ball.position.y * 2;

	// move to behind the player's paddle
	camera.position.x = paddle1.position.x - 100;
	camera.position.y += (paddle1.position.y - camera.position.y) * 0.05;
	camera.position.z = paddle1.position.z + 100 + 0.04 * (-ball.position.x + paddle1.position.x);

	// rotate to face towards the opponent
	camera.rotation.x = -0.01 * (ball.position.y) * Math.PI/180;
	camera.rotation.y = -60 * Math.PI/180;
	camera.rotation.z = -90 * Math.PI/180;
}



var ballDirX = 1, ballDirY = 1, ballSpeed = 2;
function ballUpdate(){
    //BOUNCING WALLS
  if (ball.position.y <= -fieldHeight/2 || ball.position.y >= fieldHeight/2)
  {
      ballDirY = -ballDirY;
  }

  ball.position.x +=ballDirX * ballSpeed;
  ball.position.y += ballDirY * ballSpeed;

      if (ballDirY > ballSpeed * 2)
      {
          ballDirY = ballSpeed * 2;
      }
      else if (ballDirY < -ballSpeed * 2)
      {
          ballDirY = -ballSpeed * 2;
      }

      // if ball goes off the 'left' side (Player's side)
    if (ball.position.x <= -fieldWidth/2)
    {
        cpuScore++;

        // update scoreboard HTML
        document.getElementById("scores").innerHTML = playerScore + "-" + cpuScore;

        // reset ball to center
        resetBall(2);

        // check if match over (someone scored maxScore points)
        matchScoreCheck();
    }

    // if ball goes off the 'right' side (CPU's side)
    if (ball.position.x >= fieldWidth/2)
    {
      playerScore++;

      // update scoreboard HTML
      document.getElementById("scores").innerHTML = playerScore + "-" + cpuScore;

      // reset ball to center
      resetBall(1);

      // check if match over (someone scored maxScore points)
      matchScoreCheck();
    }
}

var bounceTime = 0;
var maxScore = 5;
// checks if either player or opponent has reached 7 points
function matchScoreCheck()
{
	if (playerScore >= maxScore)
	{
		// stop the ball
		ballSpeed = 0;

		document.getElementById("scores").innerHTML = "Player wins!";
		document.getElementById("winnerBoard").innerHTML = "Refresh to play again";
    bouncePaddle(paddle1);
	}
	// else if opponent has 7 points
	else if (cpuScore >= maxScore)
	{
		// stop the ball
		ballSpeed = 0;
		// write to the banner
		document.getElementById("scores").innerHTML = "CPU wins!";
		document.getElementById("winnerBoard").innerHTML = "Refresh to play again";
    bouncePaddle(paddle2);
	}
}

function bouncePaddle(paddle){
  // make paddle bounce up and down
  bounceTime++;
  paddle.position.z = Math.sin(bounceTime * 0.1) * 10;
  // enlarge and squish paddle to emulate joy
  paddle.scale.z = 2 + Math.abs(Math.sin(bounceTime * 0.1)) * 10;
  paddle.scale.y = 2 + Math.abs(Math.sin(bounceTime * 0.05)) * 10;
}

function resetBall(looser)
{
   	// position the ball in the center of the table
	ball.position.x = 0;
	ball.position.y = 0;

  ballDirX = looser == 1 ? -1 : 1;
	ballDirY = 1;
}

var paddleSpeed = 2;
function userInput(){
	if (Key.isDown(Key.A))
	{

		if (paddle1.position.y < fieldHeight * 0.45)
		{
			paddle1DirY = paddleSpeed * 0.5;
		}
		else
		{
			paddle1DirY = 0;
			paddle1.scale.z += (10 - paddle1.scale.z) * 0.2;
		}
	}
	// move right
	else if (Key.isDown(Key.D))
	{
		// if paddle is not touching the side of table
		// we move
		if (paddle1.position.y > -fieldHeight * 0.45)
		{
			paddle1DirY = -paddleSpeed * 0.5;
		}
		// else we don't move and stretch the paddle
		// to indicate we can't move
		else
		{
			paddle1DirY = 0;
			paddle1.scale.z += (10 - paddle1.scale.z) * 0.2;
		}
	}
	// else don't move paddle
	else
	{
		// stop the paddle
		paddle1DirY = 0;
	}

	paddle1.scale.y += (1 - paddle1.scale.y) * 0.2;
	paddle1.scale.z += (1 - paddle1.scale.z) * 0.2;
	paddle1.position.y += paddle1DirY;
}
var difficulty = 1;
function enemyAi(){
      // Lerp towards the ball on the y plane
    paddle2DirY = (ball.position.y - paddle2.position.y) * difficulty;

    // in case the Lerp function produces a value above max paddle speed, we clamp it
    if (Math.abs(paddle2DirY) <= paddleSpeed)
    {
      paddle2.position.y += paddle2DirY;
    }
    // if the lerp value is too high, we have to limit speed to paddleSpeed
    else
    {
      // if paddle is lerping in +ve direction
      if (paddle2DirY > paddleSpeed)
      {
        paddle2.position.y += paddleSpeed;
      }
      // if paddle is lerping in -ve direction
      else if (paddle2DirY < -paddleSpeed)
      {
        paddle2.position.y -= paddleSpeed;
      }
    }
    // We lerp the scale back to 1
    // this is done because we stretch the paddle at some points
    // stretching is done when paddle touches side of table and when paddle hits ball
    // by doing this here, we ensure paddle always comes back to default size
    paddle2.scale.y += (1 - paddle2.scale.y) * 0.2;
}

// Handles paddle collision logic
function paddlePhysics()
{
	// PLAYER PADDLE LOGIC

	// if ball is aligned with paddle1 on x plane
	// remember the position is the CENTER of the object
	// we only check between the front and the middle of the paddle (one-way collision)
	if (ball.position.x <= paddle1.position.x + paddleWidth
	&&  ball.position.x >= paddle1.position.x)
	{
		// and if ball is aligned with paddle1 on y plane
		if (ball.position.y <= paddle1.position.y + paddleHeight/2
		&&  ball.position.y >= paddle1.position.y - paddleHeight/2)
		{
			// and if ball is travelling towards player (-ve direction)
			if (ballDirX < 0)
			{
				// stretch the paddle to indicate a hit
				paddle1.scale.y = 15;
				// switch direction of ball travel to create bounce
				ballDirX = -ballDirX;
				// we impact ball angle when hitting it
				// this is not realistic physics, just spices up the gameplay
				// allows you to 'slice' the ball to beat the opponent
				ballDirY -= paddle1DirY * 0.7;
			}
		}
	}

	// OPPONENT PADDLE LOGIC

	// if ball is aligned with paddle2 on x plane
	// remember the position is the CENTER of the object
	// we only check between the front and the middle of the paddle (one-way collision)
	if (ball.position.x <= paddle2.position.x + paddleWidth
	&&  ball.position.x >= paddle2.position.x)
	{
		// and if ball is aligned with paddle2 on y plane
		if (ball.position.y <= paddle2.position.y + paddleHeight/2
		&&  ball.position.y >= paddle2.position.y - paddleHeight/2)
		{
			// and if ball is travelling towards opponent (+ve direction)
			if (ballDirX > 0)
			{
				// stretch the paddle to indicate a hit
				paddle2.scale.y = 15;
				// switch direction of ball travel to create bounce
				ballDirX = -ballDirX;
				// we impact ball angle when hitting it
				// this is not realistic physics, just spices up the gameplay
				// allows you to 'slice' the ball to beat the opponent
				ballDirY -= paddle2DirY * 0.7;
			}
		}
	}
}
